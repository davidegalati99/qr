<x-layouts>
    <div class="container">
        <div class="row">
            <div class="col-12 m-5 text-center">
                <h1>Nuova mail</h1>
            </div>
        </div>
    </div>
    <div class="container">  
    @foreach ($contacts as $contact)
    <div class="row mt-3">
        @foreach ($contact->users as $user)
        @if ($user->id != Auth::id())
            <div class="col-6">
                <p>Contatta {{$user->name}}</p>
                <a href="mailto:{{$user->email}}">{{$user->email}}</a>
            </div>
        @endif
    @endforeach
</div>
@endforeach   
</div>
</x-layouts>
