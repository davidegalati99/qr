<x-layouts>
    

    @if (session('contactCreate.success'))
      <div class="alert alert-success my-3">
          {{session('contactCreate.success')}}    
      </div>
    @endif

    @if (session('contactCreate.error'))
    <div class="alert alert-danger my-3">
        {{session('contactCreate.error')}}    
    </div>
  @endif

      <div class="container text-center m-4">
        <div class="row">
          <div class="col-12">
            <h1>QR</h1>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-5 text-light">
            
            <form action="{{route('contact.create')}}" method="POST">
              @csrf
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email</label>
                <input type="email" class="form-control" id="emailInput" aria-describedby="emailHelp" name="email" placeholder="Scrivi l'email">
              </div>
              <button type="button" id="btn-submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    


      <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</x-layouts>