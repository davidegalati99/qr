if(document.querySelector('#btn-submit')) {
    const { default: axios } = require("axios")

let token = '';

let inputs = document.querySelectorAll('input');

inputs.forEach(input => {
    if(input.getAttribute('name') == '_token'){
        token = input.getAttribute('value');
    }
});

    const params = new URLSearchParams();
    params.append('_token', token);
    
document.querySelector('#btn-submit').addEventListener('click', function() {
    params.append('email', document.querySelector('#emailInput').value);
    axios.post('/contact/create', params)
    .then(function (response) {
        if (response.data.code == 201 || response.data.code == 200) {
        window.location.replace('/contact/success');
        }
        else {
            window.location.replace('/contact/error');
        }
      })
      .catch(function (error) {
        window.location.replace('/contact/error');
      });
    })
}

