<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [];

    use HasFactory;


    public function users() {
        return $this->belongsToMany(User::class);
    }
}
