<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function createContact(Request $request) {
        $user1 = User::find(Auth::id());
        $user2 = User::where('email', $request->email)->first();
        if(!$user2) {
            return response()->json(['code' => 500, 'message' => 'mail non valida']);
        } 
        /* $contacts1 = $user1->contacts()->get(); */
        $exist = false;
        foreach ($user1->contacts as $contact) {
            foreach ($contact->users as $user) {
                if($user->id == $user2->id){
                    $exist= true;
                    break;
                }
            }
            if($exist){
                break;
            }
        }
        if(!$exist) {
            $contact = Contact::create();
            $contact->users()->attach($user1);
            $contact->users()->attach($user2);
            return response()->json(['code' => 201, 'message' => 'contatto aggiunto nell\'elenco']);
        }
        return response()->json(['code' => 200, 'message' => 'contatto presente nell\'elenco']);
    }

    public function showContacts() {
        $contacts = Auth::user()->contacts()->get();
        return view('contacts', compact('contacts'));
    }

    public function messageContact($message) {
        if ($message == 'success') {
            return redirect(route('welcome'))->with('contactCreate.success','Contatto salvato con successo');
        }else if($message == 'error') {
            return redirect(route('welcome'))->with('contactCreate.error','Contatto non salvato, controlla l\'email se è giusta!' );
        }
    }
}
